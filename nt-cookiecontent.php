<?php
/*
Plugin Name:  NT Cookie Consent
Plugin URI:   https://www.nouveauxterritoires.fr
Version:      1.0.5
Author:       Mathieu Basili
Gitlab Plugin URI: https://gitlab.com/Mat_/wp-ntcookiecontent
*/

function panier_cookie_enqueue_script() {
	wp_enqueue_script( 'cookieconsent-nt', plugin_dir_url( __FILE__ ) . 'js/cookieconsent.js', [], "1.0", true );
	//wp_enqueue_script( 'cookieapp-nt', plugin_dir_url( __FILE__ ) . 'js/app.js', array('cookieconsent-nt'), "1.0", true );

	//wp_enqueue_style( 'cookieapp-nt', plugin_dir_url( __FILE__ ) . 'css/app.css', 'cookieconsent-nt' );
	wp_enqueue_style( 'cookieapp-css', 'https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css' );

	$couleur = get_option('couleurga');
	$codeua  = get_option('codeua');
	$lienga  = get_option('lienga');
	$textga  = get_option('texteinfoua');
	if( !$couleur ) $couleur = "#de6346";
	if( !$codeua ) $codeua = "UA-72700252-10";
	if( !$lienga ) $lienga = "/politique-confidentialite";
	if( !$textga ) $textga = 'Dans le respect de notre politique de confidentialité, nous souhaitons installer des cookies à l\'occasion de votre visite sur notre site pour en améliorer le fonctionnement et permettre d’en comprendre l\'usage.';

	wp_add_inline_script('cookieconsent-nt', '
		window.cookieconsent.initialise({
	    "palette": {
	        "popup": {
	            "background": "'.$couleur.'",
	            "text": "#ffffff"
	        },
	        "button": {
	            "background": "transparent",
	            "text": "#ffffff",
	            "border": "#ffffff"
	        }
	    },
	    "position": "bottom",
	    "type": "opt-in",
	    "content": {
	        "message": "'.$textga.'",
	        "dismiss": "J\'ai compris !",
	        "allow": "J\'accepte",
	        "link": "En savoir plus",
	        "href": "'.$lienga.'",
	        "deny": "Refuser",
	        "policy": "Vie privée",
	    },
	    onInitialise: function(status) {
	        if(status == cookieconsent.status.allow) myScripts();
	    },
	    onStatusChange: function(status) {
	        if (this.hasConsented()) {
	            console.log("consent ok");
	            myScripts();
	            }
	    }
	});
	function myScripts() {
    console.log("Cookie ok");
    (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	    })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');
	    ga(\'create\', \''.$codeua.'\', \'auto\');
	    ga(\'send\', \'pageview\');
	    ga( \'set\', \'forceSSL\', true );
	    ga( \'set\', \'anonymizeIp\', true );
	    ga( \'require\', \'displayfeatures\' );
	    ga( \'require\', \'ec\' );
	
	}
	', 'after' );
}
add_action('wp_enqueue_scripts', 'panier_cookie_enqueue_script');

// Settings Page: Cookie Consent
class cookieconsent_Settings_Page {

	public function __construct() {
		add_action( 'admin_menu', array( $this, 'wph_create_settings' ) );
		add_action( 'admin_init', array( $this, 'wph_setup_sections' ) );
		add_action( 'admin_init', array( $this, 'wph_setup_fields' ) );
	}

	public function wph_create_settings() {
		$page_title = 'Cookie Consent';
		$menu_title = 'Cookie Consent';
		$capability = 'manage_options';
		$slug = 'cookieconsent';
		$callback = array($this, 'wph_settings_content');
		$icon = 'dashicons-admin-settings';
		$position = 20;
		add_menu_page($page_title, $menu_title, $capability, $slug, $callback, $icon, $position);
	}

	public function wph_settings_content() { ?>
		<div class="wrap">
			<h1>Cookie Consent</h1>
			<?php settings_errors(); ?>
			<form method="POST" action="options.php">
				<?php
				settings_fields( 'cookieconsent' );
				do_settings_sections( 'cookieconsent' );
				submit_button();
				?>
			</form>
		</div> <?php
	}

	public function wph_setup_sections() {
		add_settings_section( 'cookieconsent_section', '', array(), 'cookieconsent' );
	}

	public function wph_setup_fields() {
		$fields = array(
			array(
				'label' => 'Code UA',
				'id' => 'codeua',
				'type' => 'text',
				'section' => 'cookieconsent_section',
			),
            array(
				'label' => 'Texte information',
				'id' => 'texteinfoua',
				'type' => 'text',
				'section' => 'cookieconsent_section',
			),
			array(
				'label' => 'Lien page rgpd',
				'id' => 'lienga',
				'type' => 'text',
				'section' => 'cookieconsent_section',
			),
			array(
				'label' => 'Couleurs principale',
				'id' => 'couleurga',
				'type' => 'color',
				'section' => 'cookieconsent_section',
			),
		);
		foreach( $fields as $field ){
			add_settings_field( $field['id'], $field['label'], array( $this, 'wph_field_callback' ), 'cookieconsent', $field['section'], $field );
			register_setting( 'cookieconsent', $field['id'] );
		}
	}

	public function wph_field_callback( $field ) {
		$value = get_option( $field['id'] );
		$placeholder = '';
		if ( isset($field['placeholder']) ) {
			$placeholder = $field['placeholder'];
		}
		switch ( $field['type'] ) {
			default:
				printf( '<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" />',
					$field['id'],
					$field['type'],
					$placeholder,
					$value
				);
		}
		if( isset($field['desc']) ) {
			if( $desc = $field['desc'] ) {
				printf( '<p class="description">%s </p>', $desc );
			}
		}
	}
}
new cookieconsent_Settings_Page();
